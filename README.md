# Toastio dashboard
This is the user dashboard front-end source code of project Toastio.

## Develop

```shell
$ npm run dev
```

## 部署到staging

1. 首先本地编译出静态文件，并上传，并生成html文件，执行如下命令

```shell
$ npm run staging
```

2. 然后将html传入git

```shell
$ git add .
$ git commit -m version
$ git push origin [你的分支名]
```

3. 执行bundle

```shell
$ bundle exec cap staging deploy
```

**`dist`里面的文件不要删，如果有冲突就解决冲突。**

## 部署到生产环境

1. 本地编译出静态文件，并上传，并生成html文件，执行如下命令

```shell
$ npm run production
```

2. 将html传入git
**注意：必须传到master分支上（如果没有权限，提PR，找晓孟合并）**

3. 找老龚部署
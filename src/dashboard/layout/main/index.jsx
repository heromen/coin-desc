/**
 * @name: Main组件
 * @description: 主layout组件
 */

import React, { Component } from 'react';
import { connect } from 'dva';
import { withRouter } from 'dva/router';
import { addLocaleData, IntlProvider } from 'react-intl';
import en from 'react-intl/locale-data/en';
import zh from 'react-intl/locale-data/zh';
import ja from 'react-intl/locale-data/ja';
import ko from 'react-intl/locale-data/ko';
import './style.scss';

import Header from '../header';

addLocaleData([...en, ...zh, ...ja, ...ko]);

class Main extends Component {
  render() {
    const { locale, messages } = this.props;
    const localePrefix = locale.split('-')[0];
    return (
      <IntlProvider locale={localePrefix} key={locale} messages={messages}>
        <div id="main">
          <Header />
          {this.props.children}
        </div>
      </IntlProvider>
    );
  }
}

function mapStateToProps({ utils }) {
  return {
    pathname: utils.pathname,
    locale: utils.locale,
    messages: utils.i18n,
  };
}

export default withRouter(connect(mapStateToProps)(Main));

// import moment from 'moment';

function getSign() {
  const d = new Date();
  return parseInt(d / 1000 / 60 / 60, 10) * 1000 * 60 * 60;
}

const querys = {
  QUERY_ACCOUNT_BASEINFO: '/web/settings.json',
  I18N: locale => `https://assets.bitrabbit.com/i18n/${__ENV__}/dashboard/${locale}.json?_=${getSign()}`,
  QUERY_BONUS_LIST: '/web/bonus.json',
  QUERY_MARKETS_PRICES: '/api/v2/markets',
  CONFIRM_BONUS: '/web/bonus/confirm.json',
  RECEIVE_BONUS: '/web/bonus/receive.json',

  // 币种承接页
  CODE_LIST: '/web/currency_infos.json',
  CODE_DETAIL: code => `/web/currency_infos/${code}/detail.json`,
};

export default querys;

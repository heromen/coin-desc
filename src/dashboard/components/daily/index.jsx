import React, { Component } from 'react';
import { connect } from 'dva';
import { FormattedMessage } from 'react-intl';
import './style.scss';
import Slogan from './components/slogan';
import Task from './components/task';
import More from './components/more';
import Rule from './components/rule';
import { Icon } from '../../../lib/antd';

class Daily extends Component {
  render() {
    return (
      <div className="daily" id="daily">
        <Slogan />
        <div className="container">
          <Task title={<FormattedMessage id="mission_daily" />} dataSource={this.props.dailyList} taskType="daily" />
          <Task title={<FormattedMessage id="mission_growth" />} dataSource={this.props.growList} taskType="growth" />
          <More dataSource={this.props.moreList} />
          <div className="receive-record"><a href="/dashboard/#/events?tab=2" target="_blank" rel="noopener noreferrer"><FormattedMessage id="event_bonus_record" /> <Icon type="arrow-right" /></a></div>
          <Rule />
        </div>
      </div>
    );
  }
}

function mapStateToProps({ task }) {
  const { dailyList, growList, moreList } = task;
  return {
    dailyList,
    growList,
    moreList,
  };
}

export default connect(mapStateToProps)(Daily);

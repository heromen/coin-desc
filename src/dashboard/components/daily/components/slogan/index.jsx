import React, { Component } from 'react';
import { connect } from 'dva';
import { FormattedMessage } from 'react-intl';

import { Button } from '../../../../../lib/antd';
import carrotImg from '../../../../../assets/images/carrot.svg';
import vntImg from '../../../../../assets/images/vns.png';
import bigCarrotImg from '../../../../../assets/images/carrot1.png';

class Slogan extends Component {
  getBonus() {
    const { bonusList } = this.props;
    const ret = bonusList.map((d, i) => <div className="slogan-sign" key={d.bonus_id}>
      <img src={i === 6 ? vntImg : carrotImg} role="presentation" className={d.state !== 'pending' ? 'slogan-bg' : ''} />
      <div className="slogan-amount">{`${d.bonus_currencies[0].amount}${d.bonus_currencies[1] ? '+' + d.bonus_currencies[1].amount : ''}`}</div>
      <div className="slogan-daily">
        {d.state === 'pending' ? d.x_day.split('_')[0] : <FormattedMessage id="mission_table_received" />}
      </div>
    </div>);
    return ret;
  }
  handleReceiveCarrot() {
    this.props.dispatch({
      type: 'task/confirmBonus',
    });
  }
  render() {
    const { bonusList, current } = this.props;
    const doneList = bonusList.filter(b => b.state === 'done');
    const firstPending = bonusList.filter(b => b.state === 'pending')[0];
    const showReceivedAmount = firstPending && firstPending.bonus_currencies[0].amount;
    const showReceivedVNTAmount = firstPending && firstPending.bonus_currencies[1] && firstPending.bonus_currencies[1].amount;
    const lastDone = doneList.length ? doneList[doneList.length - 1] : undefined;
    let isReceived = false;
    if (lastDone && lastDone.x_day_i === current.x_day_i) isReceived = lastDone.receive;
    return (
      <div id="slogan">
        <div className="container">
          <div className="slogan-left">
            <div className="slogan-title"><FormattedMessage id="mission_slogan_title" /></div>
            <div className="slogan-text"><FormattedMessage id="mission_slogan_content" /></div>
            <div className="slogan-carrot">
              {this.getBonus()}
            </div>
          </div>
          <div className="slogan-right">
            <div><img src={bigCarrotImg} role="presentation" /></div>
            {!isReceived ?
              <Button size="large" block className="slogan-btn-collect" onClick={this.handleReceiveCarrot.bind(this)}>
                <FormattedMessage id="mission_table_receive" /> ({showReceivedAmount} Carrot {showReceivedVNTAmount ? `+ ${showReceivedVNTAmount} VNT` : ''} )
              </Button> :
              <Button size="large" className="slogan-btn-collect disabled"><FormattedMessage id="mission_table_received" /></Button>
            }
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ task }) {
  const { bonusList, current } = task;
  return {
    bonusList,
    current,
  };
}

export default connect(mapStateToProps)(Slogan);

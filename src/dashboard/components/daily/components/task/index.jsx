import React, { Component } from 'react';
import { connect } from 'dva';
import { FormattedMessage } from 'react-intl';
import { Table, Button, Modal } from '../../../../../lib/antd';

class Task extends Component {
  completeTask(r) {
    const { dispatch } = this.props;
    const { resource_type: resourceType, state } = r;
    if (resourceType === 'IdDocumentVerified' && state === 'pending') {
      dispatch({
        type: 'task/queryBaseInfo',
        onSuccess: (data) => {
          if (data.id_document_state === 'verified') {
            location.reload();
          }
          window.open(r.link);
        },
      });
    } else window.open(r.link);
  }
  receiveBonus(r) {
    this.props.dispatch({
      type: 'task/receiveBonus',
      payload: {
        bonus_id: r.bonus_id,
      },
    });
  }
  render() {
    const { title, dataSource, modal, i18n, dispatch, bonusCount } = this.props;
    const columns = [
      {
        title: <FormattedMessage id="mission_table_task" />,
        key: 'name',
        dataIndex: 'name',
        width: '36%',
      },
      {
        title: <FormattedMessage id="mission_table_rewards" />,
        key: 'bonus_currencies',
        dataIndex: 'bonus_currencies',
        render: (t) => {
          let ret = null;
          if (t && Array.isArray(t) && t.length) {
            ret = t.map(cu => <span className="currency-column" key={cu.currency_code}>
              <img src={cu.icon} role="presentation" className="currency-column-img" />
              {cu.amount}{cu.currency_code}
            </span>);
          }
          return ret;
        },
        width: '35%',
      },
      {
        title: <FormattedMessage id="mission_table_progress" />,
        key: 'state',
        dataIndex: 'state',
        render: (t, r) => {
          const { taskType } = this.props;
          if (r.resource_type === 'IdDocument' || r.resource_type === 'Deposit') {
            if (t === 'need_receive' && taskType === 'daily') {
              return <Button type="primary" onClick={() => window.open('/dashboard/#/events?tab=1')}><FormattedMessage id="mission_table_receive" /></Button>;
            }
          }
          switch (t) {
            case 'pending':
              return <Button type="primary" onClick={this.completeTask.bind(this, r)}><FormattedMessage id="mission_table_complete" /></Button>;
            case 'need_receive':
              return <Button type="primary" onClick={this.receiveBonus.bind(this, r)}><FormattedMessage id="mission_table_receive" /></Button>;
            case 'done':
              return <Button type="primary" disabled ><FormattedMessage id="mission_table_done" /></Button>;
            default:
              return null;
          }
        },
      },
    ];
    let rewards = '';
    let key = 0;
    if (bonusCount === 5) {
      rewards = '140VNS';
      key = 1;
    } else if (bonusCount === 8) {
      rewards = '800VNS';
      key = 2;
    } else if (bonusCount === 11) {
      rewards = '140VNS + 100Carrot';
      key = 3;
    }
    return (
      <div className="task">
        <div className="task-title">{title}</div>
        <Table
          className="task-table"
          rowKey={r => r.bonus_id}
          columns={columns}
          pagination={false}
          dataSource={dataSource}
        />
        <Modal
          visible={modal === false}
          okText={i18n.mission_goto_forum}
          cancelText={i18n.cancel}
          title={i18n.mission_goto_forum}
          onCancel={() => dispatch({ type: 'task/updateState', payload: { modal: true } })}
          onOk={() => window.open('https://pub.bitrabbit.com/groups/welfare_club/topics/89090')}
        >
          <FormattedMessage id={`mission_more_${key}`} /> <FormattedMessage id="mission_table_rewards" />: <span>{rewards}</span>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps({ task, utils }) {
  return {
    baseInfo: task.baseInfo,
    bonusCount: task.bonusCount,
    modal: task.modal,
    i18n: utils.i18n,
  };
}

export default connect(mapStateToProps)(Task);

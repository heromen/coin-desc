import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Table } from '../../../../../lib/antd';

export default function More(props) {
  const dataSource = props.dataSource;
  const columns = [
    {
      title: <FormattedMessage id="mission_table_task" />,
      key: 'name',
      dataIndex: 'name',
      width: '36%',
    },
    {
      title: <FormattedMessage id="mission_table_rewards" />,
      key: 'reward',
      dataIndex: 'reward',
      render: (t) => {
        let ret = null;
        if (t && Array.isArray(t) && t.length) {
          ret = t.map(cu => <span className="currency-column" key={cu.icon}>
            <img src={cu.icon} role="presentation" className="currency-column-img" />
            {cu.currency_code}
          </span>);
        }
        return ret;
      },
      width: '35%',
    },
    {
      title: ' ',
      key: 'state',
      render: () => <a href="https://pub.bitrabbit.com/groups/welfare_club/topics/89090" target="_blank" rel="noopener noreferrer">前往论坛</a>,
    },
  ];
  return (
    <div className="task">
      <div className="task-title"><FormattedMessage id="mission_more" /></div>
      <Table
        className="task-table"
        rowKey={r => r.id}
        columns={columns}
        pagination={false}
        dataSource={dataSource}
      />
    </div>
  );
}

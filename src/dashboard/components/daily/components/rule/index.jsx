import React from 'react';
import { FormattedMessage } from 'react-intl';

export default function Rule() {
  return (
    <div id="rule">
      <div className="rule-title"><FormattedMessage id="mission_rule" /></div>
      <ol>
        <li><FormattedMessage id="mission_rule_1" /></li>
        <li><FormattedMessage id="mission_rule_2" /></li>
        <li><FormattedMessage id="mission_rule_3" /></li>
        <li><FormattedMessage id="mission_rule_4" /></li>
      </ol>
    </div>
  );
}

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Tooltip } from '../../../../lib/antd';
import './tag.scss';

function Flag(props) {
  const record = props.record;
  const i18n = props.i18n;
  const tags = record.tags;

  // 币名
  let coinName;
  let activityTitle;
  if (record.currency) {
    coinName = record.currency;
  } else {
    coinName = record.name.split('/')[0];
  }

  let tag;
  if (tags) {
    tag = tags.map((i) => {
      const contents = i.split('|')[0];
      const linkContent = i.split('|')[1];
      const jsonLink = linkContent && JSON.parse(linkContent).link;
      switch (window.locale) {
        case 'zh-CN':
          activityTitle = '交易送福袋';
          break;
        case 'zh-TW':
          activityTitle = '交易送福袋';
          break;
        case 'en':
          activityTitle = 'trading bag';
          break;
        default:
          break;
      }

      let tagName; // 返回的字段
      let tagContent; // 字段对应的文案
      let tagStyle; // 字段样式

      switch (contents) {
        case 'super':
          tagName = i18n.tags_super;
          tagContent = <FormattedMessage id="tags_super_content" values={{ currency: coinName, link: jsonLink }} />;
          tagStyle = 'super';
          break;
        case 'free':
          tagName = i18n.tags_free;
          tagContent = <FormattedMessage id="tags_free_content" values={{ currency: coinName, link: jsonLink }} />;
          tagStyle = 'free';
          break;
        case 'giveaways':
          tagName = i18n.tags_giveaways;
          tagContent = <FormattedMessage id="tags_giveaways_content" values={{ currency: coinName, link: jsonLink }} />;
          tagStyle = 'giveaways';
          break;
        case 'aridrop':
          tagName = i18n.tags_aridrop;
          tagContent = <FormattedMessage id="tags_aridrop_content" values={{ currency: coinName, link: jsonLink }} />;
          tagStyle = 'aridrop';
          break;
        case 'lottery':
          tagName = i18n.tags_lottery;
          tagContent = <FormattedMessage id="tags_lottery_content" values={{ currency: coinName, title: activityTitle, link: jsonLink }} />;
          tagStyle = 'lottery';
          break;
        case 'excellent':
          tagName = i18n.tags_excellent;
          tagContent = i18n.tags_excellent_content;
          tagStyle = 'excellent';
          break;
        case 'st':
          tagName = i18n.tags_st;
          tagContent = i18n.tags_st_content;
          tagStyle = 'st';
          break;
        case 'innovative':
          tagName = i18n.tags_innovative;
          tagContent = i18n.tags_innovative_content;
          tagStyle = 'innovative';
          break;
        case 'game':
          tagName = i18n.tags_game;
          tagContent = i18n.tags_game_content;
          tagStyle = 'game';
          break;
        default:
          break;
      }

      const obj = {
        tagName,
        tagContent,
        jsonLink,
        tagStyle,
      };
      return obj;
    });
  }

  return (
    <div id="tagsWrapper" onClick={e => e.stopPropagation()}>
      {tag && tag.map((value, index) => (
        <Tooltip overlayClassName="tags-tooltip-wrapper" key={index} title={<span className="tag-content">{value.tagContent}<a href={value.jsonLink} className="tag-link">{value.jsonLink}</a></span>}>
          <span className={value.tagStyle}>{value.tagName}</span>
        </Tooltip>
        )
      )}
    </div>
  );
}

export default Flag;

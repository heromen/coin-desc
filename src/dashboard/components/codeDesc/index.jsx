/* eslint react/no-danger:0 */

import React, { Component } from 'react';
import { connect } from 'dva';
import { FormattedMessage } from 'react-intl';
import { Dropdown, Menu, Icon } from '../../../lib/antd';

import './style.scss';

const Item = Menu.Item;

const isMobile = window.innerWidth < 875;

class CodeDesc extends Component {
  render() {
    const { codeList, codeDetail, prices, currencyCode } = this.props;
    let transactionArray = [];
    transactionArray = prices.filter(c => c.name.includes(currencyCode));
    const currencyInfo = codeDetail.currency_info || {};
    const detail = codeDetail.detail || {};
    const gotoCode = (item) => {
      this.props.dispatch({
        type: 'utils/goto',
        goto: `/${item.key}`,
      });
    };
    const codeMenu = (
      <Menu onClick={gotoCode} className="code-dropdown">
        {codeList.map(item => <Item key={item.code}><img width="18" src={item.icon} alt={item.code} /> {item.code}</Item>)}
      </Menu>
    );
    return (<div id="desc" className="normal-page container">
      <div>
        <div className="page-title">
          <div className="code-list">
            <Dropdown overlay={codeMenu} placement="bottomCenter" trigger={['click']}>
              <a><img width="18" src={currencyInfo.icon} alt={currencyInfo.code} /> {currencyInfo.code} <Icon type="down" /></a>
            </Dropdown>
          </div>
        </div>
        <div className="desc-content" >
          {!isMobile ? <table style={{ width: '100%' }} className="desc-table">
            <tbody>
              <tr>
                <th><FormattedMessage id="codedesc_table_max_supply" /></th>
                <th><FormattedMessage id="codedesc_table_circulating_supply" /></th>
                <th><FormattedMessage id="codedesc_table_issue_date" /></th>
                <th><FormattedMessage id="codedesc_table_issue_price" /></th>
                <th><FormattedMessage id="codedesc_table_consensus_protocol" /></th>
              </tr>
              <tr>
                <td>{detail.max_supply}</td>
                <td>{detail.circulating_supply}</td>
                <td>{detail.issue_date}</td>
                <td>{detail.issue_price}</td>
                <td>{detail.consensus_protocol}</td>
              </tr>
              <tr>
                <th><FormattedMessage id="codedesc_table_crypto_algorithm" /></th>
                <th><FormattedMessage id="codedesc_table_source_code" /></th>
                <th><FormattedMessage id="codedesc_table_white_paper" /></th>
                <th><FormattedMessage id="codedesc_table_website" /></th>
                <th><FormattedMessage id="codedesc_table_type" /></th>
              </tr>
              <tr>
                <td>{detail.cryptographic_algorithm}</td>
                <td><a href={detail.source_code} target="_blank" rel="noopener noreferrer">{detail.source_code}</a></td>
                <td><a href={detail.white_paper} target="_blank" rel="noopener noreferrer">{detail.white_paper}</a></td>
                <td><a href={detail.website} target="_blank" rel="noopener noreferrer">{detail.website}</a></td>
                <td>{detail.currency_type}</td>
              </tr>
            </tbody>
          </table> : <table style={{ width: '100%' }} className="desc-table">
            <tbody>
              <tr>
                <th><FormattedMessage id="codedesc_table_max_supply" /></th>
                <th><FormattedMessage id="codedesc_table_circulating_supply" /></th>
              </tr>
              <tr>
                <td>{detail.max_supply}</td>
                <td>{detail.circulating_supply}</td>
              </tr>
              <tr>
                <th><FormattedMessage id="codedesc_table_website" /></th>
                <th><FormattedMessage id="codedesc_table_type" /></th>
              </tr>
              <tr>
                <td><a href={detail.website} target="_blank" rel="noopener noreferrer">{detail.website}</a></td>
                <td>{detail.currency_type}</td>
              </tr>
              <tr>
                <th><FormattedMessage id="codedesc_table_issue_date" /></th>
                <th><FormattedMessage id="codedesc_table_issue_price" /></th>
              </tr>
              <tr>
                <td>{detail.issue_date}</td>
                <td>{detail.issue_price}</td>
              </tr>
              <tr>
                <th><FormattedMessage id="codedesc_table_consensus_protocol" /></th>
                <th><FormattedMessage id="codedesc_table_crypto_algorithm" /></th>
              </tr>
              <tr>
                <td>{detail.consensus_protocol}</td>
                <td>{detail.cryptographic_algorithm}</td>
              </tr>
              <tr>
                <th><FormattedMessage id="codedesc_table_source_code" /></th>
                <th><FormattedMessage id="codedesc_table_white_paper" /></th>
              </tr>
              <tr>
                <td><a href={detail.source_code} target="_blank" rel="noopener noreferrer">{detail.source_code}</a></td>
                <td><a href={detail.white_paper} target="_blank" rel="noopener noreferrer">{detail.white_paper}</a></td>
              </tr>
            </tbody>
          </table>}

          <div>
            <div className="transaction">
              <div><FormattedMessage id="codedesc_transaction" /></div>
            </div>
            <div className="transaction-content">
              {transactionArray.map((v, i) => <a href={`${location.origin}/markets/${v.name.replace('/', '_').toLowerCase()}`} target="_blank" rel="noopener noreferrer" className="transaction-item" key={i}><span>{v.name}</span></a>)}
            </div>
          </div>
          <div dangerouslySetInnerHTML={{ __html: detail.introduction }} />
        </div>
      </div>
    </div>);
  }
}

function mapStateToProps({ account, utils }) {
  const { codeList, codeDetail, currencyCode, prices } = account;
  return {
    codeList,
    showSearch: utils.showSearch,
    codeDetail,
    currencyCode,
    prices,
  };
}

export default connect(mapStateToProps)(CodeDesc);


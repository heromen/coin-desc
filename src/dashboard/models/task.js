
import pathToRegexp from 'path-to-regexp';
import fetch from '../../lib/fetch';
import { message } from '../../lib/antd';
import { QUERYS } from '../constants';

const queryBaseInfo = () => fetch.get(QUERYS.QUERY_ACCOUNT_BASEINFO);
const queryBonusList = () => fetch.get(QUERYS.QUERY_BONUS_LIST);
const confirmBonus = () => fetch.post(QUERYS.CONFIRM_BONUS);
const receiveBonus = data => fetch.put(QUERYS.RECEIVE_BONUS, data);

export default {
  namespace: 'task',
  state: {
    baseInfo: {},
    bonusList: [],
    dailyList: [],
    growList: [],
    moreList: [],
    current: {},
    bonusCount: 0,
    modal: null,
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname }) => {
        const task = pathToRegexp('/').exec(pathname);
        if (task) {
          dispatch({
            type: 'queryBonusList',
          });
        }
      });
    },
  },
  effects: {
    * queryBaseInfo({ onSuccess }, { call, put }) {
      let data = yield call(queryBaseInfo);
      if (!data.success) return;
      data = data.data;
      yield put({
        type: 'updateState',
        payload: {
          baseInfo: data,
        },
      });
      if (onSuccess) onSuccess(data);
    },
    * queryBonusList(_, { call, put, select }) {
      const i18n = yield select(({ utils }) => utils.i18n);
      const data = yield call(queryBonusList);
      if (data.success) {
        const payload = data.data;
        yield put({
          type: 'updateState',
          payload: {
            bonusList: payload.attendance,
            dailyList: payload.daily,
            growList: payload.growth,
            current: payload.current_attendance,
            bonusCount: payload.daily_bonus_count,
            modal: payload.modal,
            moreList: [{
              id: 1,
              name: i18n.mission_more_1,
              reward: [{
                currency_code: '140VNS',
                icon: 'https://assets.bitrabbit.com/upload/be17c18e-dba5-4dbf-85f7-4845609b5a9f.png',
              }],
            }, {
              id: 2,
              name: i18n.mission_more_2,
              reward: [{
                currency_code: '800VNS',
                icon: 'https://assets.bitrabbit.com/upload/be17c18e-dba5-4dbf-85f7-4845609b5a9f.png',
              }],
            }, {
              id: 3,
              name: i18n.mission_more_3,
              reward: [{
                currency_code: '140VNS',
                icon: 'https://assets.bitrabbit.com/upload/be17c18e-dba5-4dbf-85f7-4845609b5a9f.png',
              }, {
                currency_code: '100Carrot',
                icon: 'https://assets.bitrabbit.com/upload/a53a0b74-4d01-4118-b87e-f6c308aec77c.png',
              }],
            }],
          },
        });
      }
    },
    * confirmBonus(_, { call, put, select }) {
      const i18n = yield select(({ utils }) => utils.i18n);
      const data = yield call(confirmBonus);
      if (data.success) {
        message.success(i18n.mission_sign_in_success);
        yield put({
          type: 'queryBonusList',
        });
      }
    },
    * receiveBonus({ payload }, { call, put, select }) {
      const i18n = yield select(({ utils }) => utils.i18n);
      const data = yield call(receiveBonus, payload);
      if (data.success) {
        message.success(i18n.mission_table_received);
        yield put({
          type: 'queryBonusList',
        });
      }
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

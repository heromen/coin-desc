import pathToRegexp from 'path-to-regexp';
import fetch from '../../lib/fetch';
import { getDecimalCount } from '../../lib/utils';
import { QUERYS } from '../constants';

// 币种承接页
const queryCodeList = () => fetch.get(QUERYS.CODE_LIST).catch(err => err);
const queryCodeDetail = code => fetch.get(QUERYS.CODE_DETAIL(code)).catch(err => err);
const queryPrices = () => fetch.get(QUERYS.QUERY_MARKETS_PRICES);


export default {
  namespace: 'account',
  state: {
    prices: [],
    codeList: [],
    codeDetail: {},
    currencyCode: '',
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname }) => {
        const code = pathToRegexp('/:code').exec(pathname);
        if (code) {
          dispatch({
            type: 'queryCodeList',
          });
          dispatch({
            type: 'queryCodeDetail',
            payload: {
              code: code[1],
            },
          });
          dispatch({
            type: 'queryPrices',
          });
        }
      });
    },
  },
  effects: {
    * queryPrices({ onSuccess }, { call, put }) {
      const data = yield call(queryPrices);
      let prices = [];
      if (data && data.length > 0) {
        prices = data.map((price) => {
          const ticker = price.ticker;
          const tags = price.tags;
          const last = parseFloat(ticker.last);
          const open = parseFloat(ticker.open);
          const change = open === 0 ? 0 : (last - open) / open;
          const down = change < 0;
          const fixed = getDecimalCount(price.bid_config.price_minmov);
          const volumeFixed = getDecimalCount(price.bid_config.min_amount);
          const favorite = price.favorite;
          const _ret = {
            id: price.id,
            name: price.name,
            ...ticker,
            tags,
            change,
            down,
            favorite,
          };
          ['buy', 'high', 'last', 'low', 'open', 'sell'].forEach((key) => {
            _ret[key] = parseFloat(_ret[key]).toFixed(fixed);
          });
          _ret.volume = parseFloat(_ret.volume).toFixed(volumeFixed);
          return _ret;
        });
      }
      if (onSuccess) onSuccess();
      yield put({
        type: 'updateState',
        payload: {
          prices,
        },
      });
    },
    * queryCodeDetail({ payload }, { call, put }) {
      const data = yield call(queryCodeDetail, payload.code.toLowerCase());
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            codeDetail: data.data,
            currencyCode: payload.code,
          },
        });
      }
    },
    * queryCodeList(_, { call, put }) {
      const data = yield call(queryCodeList);
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            codeList: data.data.currencys,
          },
        });
      }
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

import utils from './utils';
import task from './task';
import account from './account';

const reducers = {
  utils,
  task,
  account,
};

export default reducers;

export function getPairPrices(markets, pairName) {
  const _pairName = pairName.toUpperCase();
  const pair = markets.filter(p => p.name === _pairName)[0];
  if (pair) {
    return {
      last_cny: pair.last_cny,
      last_usdt: pair.last_usdt,
    };
  }
  return undefined;
}

export function getUnitPrices(markets, unit) {
  const _unit = unit.toUpperCase();
  if (_unit === 'USDT') {
    return {
      last_cny: '6.3',
      last_usdt: '1',
    };
  }
  const pair = markets.filter(p => p.name.indexOf(_unit + '/') === 0)[0];
  if (pair) {
    return {
      last_cny: pair.last_cny,
      last_usdt: pair.last_usdt,
    };
  }
  return {
    last_cny: '0',
    last_usdt: '0',
  };
}

export function getUnitValuePrice(markets, unit, sign) {
  const prices = getUnitPrices(markets, unit);
  const _sign = sign.toUpperCase();
  let btcPrices;
  switch (_sign) {
    case 'USDT':
      return prices.last_usdt;
    case 'CNY':
      return prices.last_cny;
    case 'BTC':
      btcPrices = getPairPrices(markets, 'BTC/USDT');
      if (btcPrices) {
        return (prices.last_usdt / btcPrices.last_usdt).toString();
      }
      return '0';
    default:
      break;
  }
  return undefined;
}

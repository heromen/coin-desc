export function reducePrices(data) {
  let ret = {};
  data.forEach((node) => {
    const names = node.name.split('/');
    if (!ret[names[1]]) {
      ret[names[1]] = [];
    }
    ret[names[1]].push({
      ...node,
      currency: names[0],
    });
  });
  ret = Object.keys(ret).map(k => ({
    market: k,
    currencies: ret[k],
  }));
  ret.sort((a, b) => {
    if (a.market < b.market) return -1;
    if (a.market === b.market) return 0;
    return 1;
  });
  return ret;
}

export function getDecimalCount(d) {
  let nums = d.toString();
  if (nums.length === 0) return 0;
  nums = nums.split('.');
  if (nums.length === 1) return 0;
  return nums[1].length;
}

export function getSMSInputWidths(locale) {
  if (locale === 'en') return ['55%', '45%'];
  return ['70%', '30%'];
}

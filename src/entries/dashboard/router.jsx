import React from 'react';
import { routerRedux, Route, Switch, Redirect } from 'dva/router';

// 优先加载antd样式
import '../../lib/antd/style.less';

import Main from '../../dashboard/layout/main';  // 主视图
// import Daily from '../../dashboard/components/daily'; // 交易市场
import CodeDesc from '../../dashboard/components/codeDesc';

const { ConnectedRouter } = routerRedux;

function welcome() {
  if (window.console && window.console.log) {
    window.console.log('%cFBI WARNING', 'color:#fff;font-size:40px;font-weight:600;background-color:red');
    window.console.log('%c网络一线牵，珍惜这段缘', 'text-shadow:3px 1px 1px grey;font-size:20px');
    window.console.log('请将简历投至：\n%chr@bitrabbit.com', 'color:red;text-shadow:3px 1px 1px pink;font-size:20px');
    window.console.log('%c请不要在开发者工具控制台内执行任何代码，这有可能对网站数据造成不必要的麻烦，给你造成损失哦！', 'font-size:16px');
  }
}

welcome();

export default function Routers({ history }) {
  return (
    <ConnectedRouter history={history}>
      <Main>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/BTC" />} />
          {/* <Route exact path="/assets/" render={() => <Redirect to="/assets/BTC" />} /> */}
          <Route exact path="/:code" component={CodeDesc} />
        </Switch>
      </Main>
    </ConnectedRouter>
  );
}
